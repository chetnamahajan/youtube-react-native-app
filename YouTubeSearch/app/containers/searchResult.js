'use strict'
import React, {Component} from 'react';
import {
	View,
	ListView,
	StyleSheet,
	TextInput,
	Image,
	Platform,
	TouchableNativeFeedback,
	ScrollView,
	Text,
	ActivityIndicatorIOS,
	ProgressBarAndroid,
	Alert,
	TouchableOpacity
} from 'react-native';
import { RootContainer } from 'react-relay'
import * as RelayComponent from './relayComponent';
import { connect } from 'react-redux';
import { playVideo, searchYouTubeAPI, addToFavourites, searchResult, autocomplete, actions } from '../actions/youTubeActions';
import {bindActionCreators} from 'redux';
import Button from 'apsl-react-native-button';
import FavouriteMenu from '../components/favouriteMenu';


let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class SearchResult extends Component {
	constructor(props) {
	  super(props);
	  this.state = { 
	  	keyword: "", 
	  	close: false, 
	  	videoDetails: this.props.videoDetails, 
	  	dataSource:  ds.cloneWithRows(this.props.videoDetails.searchSuggestion) 
	  };
	}

	handleSubmit(event) {
		this.setState({close: true});
		if(this.state.keyword==="")
			Alert.alert("Please enter something to search.");
		else{
			this.props.searchYouTubeAPI(this.state.keyword);
			this.props.navigator.replace({
			  title: 'searchResult',
			  id : 'searchResult',
			});
		}
	}

	componentWillReceiveProps(nextProps) {
	  this.setState({ 
	    videoDetails: nextProps.videoDetails,
	    dataSource : ds.cloneWithRows(nextProps.videoDetails.searchSuggestion)
	  });
	}

	handleRowPress(rowData) {
	  this.setState({keyword: rowData});
	  this.props.actions(rowData);
	  this.props.navigator.replace({
	    title: 'searchResult',
	    id : 'searchResult',
	  });
	}

	renderRow(rowData) {
	  return (
	    <View style={styles.rowButton}>
	      <View style={styles.rowWrapper}>
	        <TouchableOpacity onPress={() => this.handleRowPress(rowData)}>
	          <Text>{ rowData }</Text>
	        </TouchableOpacity>
	      </View>
	    </View>
	  );
	}


	render() {

		let { UserRoute, UserInfo } = RelayComponent;
		let { searchKeyword } = this.state.videoDetails;
		return (
			<View style={styles.container}>
				<FavouriteMenu close={this.state.close} {...this.props}/>
				<View style={styles.search}>
					<TextInput
						style={styles.searchInput}
						value={this.state.keyword}
						onFocus={()=> this.setState({close: true})}  
						onChangeText={(keyword) =>{ 
		                    this.setState({keyword, close: true});
		                    this.props.autocomplete(keyword);
		                }}
						onSubmitEditing={this.handleSubmit.bind(this)}
						placeholder="Search for videos" />
					<Button  background={(Platform.OS === 'android') ? 
							TouchableNativeFeedback.Ripple('#f39c12', true) : null} 
							style={styles.searchButton}
							onPress={this.handleSubmit.bind(this)}>
						<Image
						  	style={styles.icon}
						  	source={require('../images/search.png')} />
					</Button>
				</View>
				<View>
				   {this.props.videoDetails.searchSuggestion.length>0 && this.state.keyword.length>0 ?
				     <ListView
				       style={{height:120, marginLeft:10, marginRight: 10}}
				       dataSource={this.state.dataSource}
				       renderRow={this.renderRow.bind(this)}/>: null}
				</View>	
				<RootContainer
				  Component={UserInfo}
				  route={new UserRoute({Keyword: searchKeyword})}
				  renderLoading={function() {
				    if (Platform.OS === 'ios')
				      return (
				        <View style={styles.loader}>  
				              <ActivityIndicatorIOS
				                style={{height: 80}}
				                size="large"/> 
				        </View>
				    )
				    else
				      return (
				        <View style={styles.loader}>  
				              <ProgressBarAndroid color="gray"/>
				        </View>
				    )
				  }}
				  renderFetched={(data) => <ScrollView><UserInfo videoDetails={this.state.videoDetails} {...this.props} {...data} /></ScrollView>}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F1F1F1',
		padding: 10
	},
	loader:{
		flexDirection:'row',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf:'center'
	},
	search:{
		flexDirection: 'row',
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 2,
		marginTop:10
	},
	searchInput: {
		backgroundColor: '#ffffff',
		height: 60,
		padding: 10,
		borderRadius: 0,
		flex:1,
	},
	searchButton: {
		borderColor: 'transparent',
		backgroundColor: '#ffffff',
		borderRadius: 0,
		borderWidth: 3,
		padding:10,
		height: 60,
	},
	icon: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		height: 30,
		width: 30
	},
	rowWrapper: {
	  flexDirection: 'column',
	  justifyContent: 'center',
	  flex:1
	},
	rowButton: {
	  padding: 10,
	},
})

export default connect(state => ({
	  videoDetails: state.videoDetails
	}),
	(dispatch) => ({
	  actions: bindActionCreators(searchYouTubeAPI, dispatch),	
	  playVideo: bindActionCreators(playVideo, dispatch),
	  searchYouTubeAPI: bindActionCreators(searchYouTubeAPI, dispatch),
	  addToFavourites: bindActionCreators(addToFavourites, dispatch),
	  autocomplete: bindActionCreators(autocomplete,dispatch),
	  searchResult: bindActionCreators(searchResult,dispatch)
	})
)(SearchResult);