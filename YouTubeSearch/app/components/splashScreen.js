'use strict'
import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

let {height, width} = Dimensions.get('window');

export default class SplashScreen extends Component{
  componentWillMount() {
    setTimeout( () => {
      this.props.navigator.resetTo({
        title: 'Home',
        id : 'home'
      })
    }, 1000);  
  }

  render() {

    return(
      <View style={ styles.container }>
            <Image source={require('../images/logo.png')} style={ styles.splash }></Image>
      </View>  
    );
  }
}

let styles = StyleSheet.create({
  container : {
      flex : 1
  },
  splash : {
      width : width,
      height : height
  }
});

