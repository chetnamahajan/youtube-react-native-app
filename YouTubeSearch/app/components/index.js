'use strict';

import React, {Component} from 'react';
import {
  AppRegistry,
  BackAndroid,
  Navigator,
  StyleSheet,
  View,
  ActivityIndicatorIOS,
  Platform,
  Text,
  AsyncStorage
} from 'react-native';
import Route from './route';
import ToolbarAndroid from 'ToolbarAndroid';


let _navigator;
BackAndroid.addEventListener('hardwareBackPress', () => {
  if (_navigator && _navigator.getCurrentRoutes().length === 2) {
    _navigator.resetTo({
      title: 'Home',
      id : 'home',
    });
    return true;
  }
  if (_navigator && _navigator.getCurrentRoutes().length > 1) {
    _navigator.pop();
    return true;
  }
  return false;
});


export default class Index extends Component{

  componentWillMount() {
    console.disableYellowBox = true;
    console.disableRedBox = true;
  }

  render() {
    return (
      <Navigator
        style={styles.container}
        initialRoute={{
        id : 'splashScreen',
        title: 'splashScreen'
      }}
        configureScene={() => Navigator.SceneConfigs.FloatFromRight}
        renderScene={this.renderScene}/>
    );
  }

  renderScene(route, navigator) {
    _navigator = navigator;
   return (
    <View {...route.props} {...navigator.props}> 
     <Route
       ref={this.onLoadedScene}
       route={route}
       navigator={navigator}
       {...route.props}/>
    </View>   
   );
  }

};

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  }
});
