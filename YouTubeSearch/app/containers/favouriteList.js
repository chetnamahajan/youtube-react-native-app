'use strict'
import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	ListView,
	Image,
	ProgressBarAndroid
} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { playVideo, addToFavourites } from '../actions/youTubeActions';
import RenderRow from '../components/renderRow';

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class FavouriteList extends Component {
	constructor(props) {
	    super(props);
		this.state = { 
			loading: true,
			dataSource: ds.cloneWithRows([]), 
			favourites: null
		};
	}

	componentWillMount(){
		this.props.videoDetails.favourites.done((data)=>{
			this.setState({dataSource: ds.cloneWithRows(data), loading: false, favourites: data});
		});
	}

	componentWillReceiveProps(nextProps) {
	  console.log("nextProps",nextProps);	
	  this.setState({ 
	    dataSource : ds.cloneWithRows(nextProps.videoDetails.favourites),
	    favourites: nextProps.videoDetails.favourites 
	  });
	}

	renderRow(rowData) {
		return (
		  <RenderRow  routePage='favouriteList' favourites={this.state.favourites} rowData={rowData} {...this.props}/>
		)
	}

	render() {

		if(this.state.loading)
			return(
				<View style={styles.loader}>  
				      <ProgressBarAndroid color="gray"/>
				</View>
			);


		if(this.state.favourites.length<1){
			return(
				<View style={styles.emptycontainer}>
					<Image
					  style={styles.icon}
					  source={require('../images/no-item.png')} />
					<Text>No items in favourite list.</Text>
				</View>
			);
		}

		return (
			<View>
			    <ListView
			      style={styles.items}
			      dataSource={this.state.dataSource}
			      renderRow={this.renderRow.bind(this)}/>
			</View>    
		);
	}
}

var styles = StyleSheet.create({
	emptycontainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
	},
	loader:{
		flexDirection:'row',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf:'center'
	},
	items: {
	  flex: 1,
	  backgroundColor: '#F1F1F1',
	  padding: 10,
	},
	icon: {
	  width: 20,
	  height: 20,
	}
})

export default connect(state =>({
    videoDetails: state.videoDetails
  }),
  (dispatch) => ({
    playVideo: bindActionCreators(playVideo, dispatch),
    addToFavourites: bindActionCreators(addToFavourites, dispatch)
  })
)(FavouriteList);