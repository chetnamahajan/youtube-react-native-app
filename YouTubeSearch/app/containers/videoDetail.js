'use strict'
import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	WebView,
	Dimensions,
	TouchableHighlight,
	Image,
	ActivityIndicatorIOS,
	ProgressBarAndroid,
	Platform,
	TouchableOpacity,
	AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import {unitFormat} from '../components/globalFunctions';
import FavouriteMenu from '../components/favouriteMenu';
import { addToFavourites } from '../actions/youTubeActions';
import YouTube from 'react-native-youtube';


class VideoDetails extends Component {

	constructor(props) {
	  super(props);
	  this.state = {
	  	video: this.props.videoDetails.playVideo,
	  	favourites: this.props.videoDetails.favourites,
	  	isReady: false,
	  	status: null,
	  	quality: null,
	  	error: null,
	  	isPlaying: true,
	  	data: []
	  };
	}

	async getValuesFromLocalStorage(state, key) {
	  try {
	    let value = await AsyncStorage.getItem(key);
	    if (value !== null){
	      this.setState({[state]: JSON.parse(value)});
	      console.log(value,"in value", JSON.parse(value));
	    } else {
	      console.log('Initialized with no selection on disk.');
	    }
	  } catch (error) {
	    console.log(error);
	  }
	}  

	componentWillMount(){
		this.getValuesFromLocalStorage('data','favourites').done(()=>{
			console.log("this.state.data",this.state.data);
		});
	}

	handleFavouritePress(video) {
	  this.props.addToFavourites(video);
	}

	componentWillReceiveProps(nextProps) {
	  this.setState({ 
	    video: nextProps.videoDetails.playVideo,
	    favourites: nextProps.videoDetails.favourites
	  });  
	}


	render() {
		const {video} = this.state;
		const date = moment(video.publishDateTime).format('MMMM Do YYYY');
		const views = unitFormat(video.statistics[0].views);
		const likes = unitFormat(video.statistics[0].likes);
		const dislikes = unitFormat(video.statistics[0].dislikes);

		let favourite = null;
		let flag=0;   
	 
		if(this.state.favourites.length>0) {
		  favourite = this.state.favourites.map((favourites)=>{
		    if(favourites.videoId===video.videoId){
		      flag=1;
		      return (<Image
		        style={styles.icon}
		        source={require('../images/favourite.png')} />)
		    }
		  });     
		}  

		if(!flag)
		  favourite = <Image
		      style={styles.icon}
		      source={require('../images/not-favourite.png')} />;
	  

		console.log(this.state.video);
		return (
			<View style={styles.container}>
				<FavouriteMenu close={this.state.close} {...this.props}/>
				<View style={styles.webview}>
					<YouTube
			             videoId={video.videoId}
			             play={this.state.isPlaying}
			             hidden={false}
			             playsInline={true}
			             apiKey="AIzaSyBasB_OlCFjWEcN4q0YRRpzyvwqqb9Lark"
			             onReady={(e)=>{this.setState({isReady: true})}}
			             onChangeState={(e)=>{this.setState({status: e.state})}}
			             onChangeQuality={(e)=>{this.setState({quality: e.quality})}}
			             onError={(e)=>{this.setState({error: e.error})}}
			             style={{alignSelf: 'stretch', height: 250, backgroundColor: 'black', marginVertical: 10}}
			        />
				</View>
				<View style={styles.content}>
					<View style={{flexDirection:'row', flexWrap: 'wrap'}}>
						<Text numberOfLines={5} style={[styles.title, {width: 299}]}>{video.videoTitle}</Text>
						<TouchableOpacity style={[styles.favourite]} onPress={() => this.handleFavouritePress(video)}>
						  { favourite }
						</TouchableOpacity>
					</View>	
					<Text>Published on {date}</Text>
					<Text style={styles.tag}>{views} views</Text>
					<View style={styles.rowWrapper}>
					  <Image
					    style={styles.icon}
					    source={require('../images/likes.png')} />
					  <Text style={styles.tag}>{likes}</Text>  
					  <Image
					    style={[styles.icon,{marginTop:7}]}
					    source={require('../images/dislikes.png')} />  
					  <Text style={styles.tag}>{dislikes}</Text>  
					</View> 
				</View>	
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
		backgroundColor: '#F1F1F1'
	},
	webview: {
		height: 290
	},
	content:{
		marginTop: 10,
		marginRight: 10,
		marginLeft: 10
	},
	title: {
		color: '#808080',
		fontWeight: 'bold',
		fontSize: 15,
		marginBottom:5
	},
	tag: {
		color: '#A8A8A8',
		fontSize: 13,
		marginTop: 5,
		marginBottom:5,
		marginLeft:3,
		marginRight:10,
		paddingBottom:5
	},
	icon: {
		width: 20,
		height: 20,
	},
	rowWrapper: {
		flex: 1,
	    flexDirection: 'row',
	    justifyContent: 'flex-start',
	    alignItems: 'flex-start',
	    alignSelf: 'flex-start',
	},
	favourite: {
	  flexDirection: 'row',
	  justifyContent: 'flex-end',
	  alignItems: 'flex-start',
	 
	}
})

export default connect(state => ({
	  videoDetails: state.videoDetails
	}),
	(dispatch) => ({
	  addToFavourites: bindActionCreators(addToFavourites, dispatch)
	})
)(VideoDetails);