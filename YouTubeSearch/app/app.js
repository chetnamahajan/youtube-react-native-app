import React, {Component} from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import * as reducers from './reducers';
import Index from './components/index';
import CheckInternet from './components/checkInternet';
import RequiresConnection from 'react-native-offline-mode';

const createStoreWithMiddleware = applyMiddleware(thunk, promise)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);


class App extends Component {
  componentWillMount() {
    console.disableYellowBox = true;
    console.disableRedBox = true;
  }
	
  render() {
    return (
      <Provider store={store}>
        <Index/>
      </Provider>
    );
  }
}

export default RequiresConnection(App, CheckInternet);
