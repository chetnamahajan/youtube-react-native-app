'use strict'
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  ListView,
  Image,
  StyleSheet,
  Alert,
  AsyncStorage,
  ProgressBarAndroid
} from 'react-native';
import moment from 'moment';
import {sortByLikesPercentage, sortByViews, unitFormat} from '../components/globalFunctions';


export default class RenderRow extends Component {
  constructor(props) {
  	super(props);
    this.state = { 
      rowData: this.props.rowData, 
      favourites: this.props.favourites,
      routePage: this.props.routePage,
      recentVideos: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ 
      rowData: nextProps.rowData, 
      favourites: nextProps.favourites,
      routePage: nextProps.routePage
    });
  }


  async getValuesFromLocalStorage(state, key) {
    try {
      let value = await AsyncStorage.getItem(key);
      if (value !== null){
        this.setState({[state]: JSON.parse(value)});
      } else {
        console.log('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log(error);
    }
  }

  async setValuesToLocalStorage(data, key){
    try {
      await AsyncStorage.removeItem(key);
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
      console.log(error);
    }
  }

  handleRowPress(rowData) {
    this.getValuesFromLocalStorage("recentVideos", "recent").done(()=>{
      let arr=[], flag=0;
      if(this.state.recentVideos!==null){
        arr=this.state.recentVideos;
        for(let i=0; i<this.state.recentVideos.length; i++)
          if(arr[i].videoId===rowData.videoId){
            arr[i].viewCount=arr[i].viewCount+1;
            flag=1;
          }
      }
      if(!flag){
        rowData.viewCount=1;
        arr.push(rowData);
      }


      sortByViews(arr);
              
      this.setValuesToLocalStorage(arr, "recent").done(()=>{
        this.props.playVideo(rowData);
        this.props.navigator.push({
          title: 'videoDetail',
          id : 'videoDetail',
        });
      });
    });

  }

  handleFavouritePress(rowData) {
    this.props.addToFavourites(rowData);
  }

  render () {
    const {rowData}=this.state;
    const date = moment(rowData.publishDateTime).format('MMM Do YY');
    const views = unitFormat(rowData.statistics[0].views);
    const likes = unitFormat(rowData.statistics[0].likes);
    const dislikes = unitFormat(rowData.statistics[0].dislikes);

    let favourite;
    let flag=0;      
    if(this.state.favourites.length>0) {
      favourite = this.state.favourites.map((video)=>{
        if(rowData.videoId===video.videoId){
          flag=1;
          return (<Image
            style={styles.icon}
            source={require('../images/favourite.png')} />)
        }
      });     
    }  

    if(!flag)
      favourite = <Image
          style={styles.icon}
          source={require('../images/not-favourite.png')} />;

    return (
      <View style={styles.row}>
          <View style={styles.rowWrapper}>
            <TouchableOpacity style={styles.columnImage} onPress={() => this.handleRowPress(rowData)}>
              <Image
                style={styles.thumbnail}
                source={{uri: rowData.mediumThumbnail}} />  
            </TouchableOpacity> 
            <TouchableOpacity style={styles.columnInfo} onPress={() => this.handleRowPress(rowData)}>
              <Text numberOfLines={2} style={styles.title}>{rowData.videoTitle}</Text>
              <Text style={[styles.tag]}>Published: {date}</Text> 
              <Text style={styles.tag}>{views} views</Text>
              <View style={styles.rowInfo}>
                <Text style={styles.tag}>{likes}</Text>
                <Image
                  style={[styles.icon,{marginLeft: 4, marginRight:5}]}
                  source={require('../images/likes.png')} />
                <Text style={styles.tag}>{dislikes}</Text>
                <Image
                  style={[styles.icon, {marginTop:7}]}
                  source={require('../images/dislikes.png')} />  
              </View>  
            </TouchableOpacity> 
            <TouchableOpacity style={styles.columnFavourite} onPress={() => this.handleFavouritePress(rowData)}>
              { favourite }
            </TouchableOpacity>  
          </View>
      </View>
    )
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  items: {
    flex: 1,
    backgroundColor: '#F1F1F1',
    padding: 10,
  },
  tag: {
    color: '#A8A8A8',
    fontSize: 13,
    marginTop: 5,
  },
  row: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    marginBottom: 12,
    padding: 10
  },
  rowWrapper: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  rowInfo: {
    flexDirection: 'row',
  },
  title: {
    color: '#808080',
    fontWeight: 'bold',
    fontSize: 15,
  },
  thumbnail: {
    width: 150,
    height: 120
  },
  icon: {
    width: 20,
    height: 20,
  },
  columnImage: {  
    width: 130,
    height: 120,
  },
  columnInfo: {  
    width: 140,
    height: 120,
    marginLeft:5
  },
  columnFavourite: {  
    width: 25,
    height: 25,
  },
})

