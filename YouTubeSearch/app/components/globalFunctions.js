import unitize from 'unitize';

export function sortByViews(videoList){
  console.log(videoList);
    return videoList.sort(function (a, b) {
      if (parseInt(a.viewCount) < parseInt(b.viewCount)) {
        return 1;
      }
      if (parseInt(a.viewCount) > parseInt(b.viewCount)) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
}

export function sortByViewsToRender(videoList){
    return videoList.sort(function (a, b) {
      let first=a.statistics[0].views;
      let second=b.statistics[0].views;

      if (parseInt(first) < parseInt(second)) {
        return 1;
      }
      if (parseInt(first) > parseInt(second)) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
}

export function sortByLikesPercentage(videoList){
  
  return videoList.sort(function (a, b) {
    let first=a.statistics[0];
    let second=b.statistics[0];
    let firstArg=parseInt(first.likes)/parseInt(first.dislikes); 
    let secondArg=parseInt(first.likes)/parseInt(second.dislikes);
    if (firstArg < secondArg) {
      return 1;
    }
    if (firstArg > secondArg) {
      return -1;
    }
    // a must be equal to b
    return 0;

  });
}

export function unitFormat(value){
  return unitize(value).precision(2).toString().toUpperCase();
}

export function titleCase(str) {
  let newstr = str.split(" ");
  for(let i=0;i<newstr.length;i++){
    if(newstr[i] == "") continue;
    let copy = newstr[i].substring(1).toLowerCase();
    newstr[i] = newstr[i][0].toUpperCase() + copy;
  }
    newstr = newstr.join(" ");
    return newstr;
}  