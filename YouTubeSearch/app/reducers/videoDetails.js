import * as types from '../constants/actionTypes';
import { sortByLikesPercentage, sortByViewsToRender } from '../components/globalFunctions';
import {  AsyncStorage } from 'react-native';

let favourites = initializeValuesFromLocalStorage("favourites");

const initialState = {
  searchItems: [],
  searchKeyword: null,
  searchSuggestion: [],
  playVideo: null,
  favourites: favourites,
};

async function setValuesToLocalStorage(data, key){
  try {
    await AsyncStorage.removeItem(key);
    await AsyncStorage.setItem(key, JSON.stringify(data));
  } catch (error) {
    console.log(error);
  }
}

async function getValuesFromLocalStorage(key) {
  try {
    let value = await AsyncStorage.getItem(key);
    if (value !== null){
      favourites = JSON.parse(value);
      console.log(value,"in value", JSON.parse(value));
    } else {
      console.log('Initialized with no selection on disk.');
    }
  } catch (error) {
    console.log(error);
  }
}  

async function initializeValuesFromLocalStorage(key) {
  try {
    let value = await AsyncStorage.getItem(key);
    console.log("value",value);
    if (value !== null){
      return JSON.parse(value);
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }  

} 

export default function videoDetails(state = initialState, action) {
  switch (action.type) {
    case types.AUTOCOMPLETE_SEARCH: 
      return { ...state, searchSuggestion: action.payload}; 

    case types.SEARCH:
      return { ...state, searchKeyword: action.payload };

    case types.SEARCH_RESULT:{
      let data = null;
      data = sortByViewsToRender(action.payload);
      return { ...state, searchItems: data };  
    }

    case types.PLAY_VIDEO:
      return { ...state, playVideo: action.payload };  

    case types.ADD_TO_FAVOURITE: {
      let arr=state.favourites;
      let arr2=[];
      let flag=0;

      console.log(arr,state,action.payload);

      for(let i=0;i<arr.length;i++){
        if(arr[i].videoId===action.payload.videoId){
          flag=1;
          delete arr[i];

        }
        else{
          flag=2;
          arr2.push(arr[i]);
        }
      }
      
      if(flag==0 || flag==2)
        arr2.push(action.payload);

      // getValuesFromLocalStorage("favourites").done(()=>{
      //   console.log(favourites.length,"favourites");
      //   let arr3=favourites, arr4=[], flag3=0;
      
      //   for(let i=0;i<favourites.length;i++){
      //     if(favourites[i].videoId===action.payload.videoId){
      //       flag3=1;
      //       delete favourites[i];
      //     } else{
      //       flag3=2;
      //       arr4.push(arr3[i]);
      //     }
      //   }

      //   if(flag3==0 || flag3==2)
      //     arr4.push(action.payload);
      
      //   setValuesToLocalStorage(arr4,"favourites").done(()=>{

      //   });
      // });

      return { ...state, favourites: arr2 };
       
    }
      
    default:
      return state;
  }
}
