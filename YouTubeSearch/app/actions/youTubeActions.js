import * as types from '../constants/actionTypes';

export function searchYouTubeAPI(keyword) {
  return {
    type: types.SEARCH,
    payload: keyword
  };
}

export function searchResult(data) {
  return {
    type: types.SEARCH_RESULT,
    payload: data
  };
}

export function playVideo(videoId) {
  return {
    type: types.PLAY_VIDEO,
    payload: videoId
  };
}

export function addToFavourites(videoData) {
  return {
    type: types.ADD_TO_FAVOURITE,
    payload: videoData
  };
}

export function autocomplete(keyword) {
  let request = [];
  if(keyword.length>0)
    request = fetch(`http://suggestqueries.google.com/complete/search?client=youtube&ds=yt&client=firefox&q=${keyword}`)
                    .then((response) => response.json())
                    .then((responseJson) => {
                      return responseJson[1];
                    })
                    .catch((error) => {
                      console.error(error);
                    });
  return {
    type: types.AUTOCOMPLETE_SEARCH,
    payload: request
  };
}


