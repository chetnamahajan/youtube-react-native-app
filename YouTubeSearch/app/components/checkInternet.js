'use strict'
import React, {Component} from 'react';
import {
	View,
	Text,
	TextInput,
	Image,
	StyleSheet
} from 'react-native';

import Button from 'apsl-react-native-button';


export default class Search extends Component {
	constructor(props) {
	  super(props);
	}


	render() {
    
		return (
			<View style={styles.emptycontainer}>
        <Text style={styles.tag}>No Internet Connection.</Text>
      </View>
		)
	}
}



var styles = StyleSheet.create({
	emptycontainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  tag: {
    color: '#A8A8A8',
    fontSize: 20,
    fontWeight: 'bold'
  },  
  icon: {
    width: 30,
    height: 30
  }
})


