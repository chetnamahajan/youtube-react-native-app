'use strict'
import React, {Component} from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet
} from 'react-native';

import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

export default class favouriteMenu extends Component {
	constructor(props) {
	  super(props);
    this.state = {
      close: this.props.close
    }
	}

  navigateTo(value) {
    if (value === 'favourite') {
      this.refs.MenuContext.closeMenu(); 
      this.props.navigator.push({
        title: 'Favourite List',
        id : 'favouriteList',
      });
    } 
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ 
      close : nextProps.close
    });
  }

	render() {
    if(this.state.close===true){
      this.refs.MenuContext.closeMenu(); 
    }

		return (
      <View>
       <MenuContext style={{height:35}} ref="MenuContext">
 			  <Menu style={{marginLeft:-10 }} onSelect={this.navigateTo.bind(this)}>
           <MenuTrigger style={{marginLeft:10}}>
             <Image
               style={styles.ellipsis}
               source={require('../images/side-menu.png')} />
           </MenuTrigger>
           <MenuOptions style={styles.menuOptions}>
             <MenuOption value="favourite">
               <Text>Favourite Videos</Text>
             </MenuOption>
           </MenuOptions>
         </Menu>
       </MenuContext>
      </View>
		)
	}
}



var styles = StyleSheet.create({
  ellipsis: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    height: 25,
    width: 25
  },
  menuOptions:{
    height:35
  },
})
