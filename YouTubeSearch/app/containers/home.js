'use strict'
import React, {Component} from 'react';
import {
	View,
	Text,
	TextInput,
	Image,
	StyleSheet,
	Alert,
	Platform,
	TouchableNativeFeedback,
  TouchableOpacity,
  ListView,
  AsyncStorage,
  NetInfo
} from 'react-native';
import { searchYouTubeAPI, autocomplete, playVideo } from '../actions/youTubeActions';
import FavouriteMenu from '../components/favouriteMenu';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import Button from 'apsl-react-native-button';
import Modal from 'react-native-simple-modal';
import {sortByLikesPercentage, sortByViews} from '../components/globalFunctions';

let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class Search extends Component {
	constructor(props) {
	  super(props);
    let data = Array.apply(null, {length: 20}).map(Number.call, Number);
	  this.state = { 
      keyword: "",
      dataSource:  ds.cloneWithRows(this.props.videoDetails.searchSuggestion),
      rows: [],
      open: false,
      removeVideoId: null,
      suggestion: null,
      close: false
    };
	}

  componentWillMount(){
    this.getValuesFromLocalStorage('rows','recent');
  }

  async getValuesFromLocalStorage(state, key) {
    try {
      let value = await AsyncStorage.getItem(key);
      if (value !== null){
        this.setState({[state]: JSON.parse(value)});
      } else {
        console.log('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log(error);
    }
  }  

  componentWillReceiveProps(nextProps) {
    this.setState({ 
      dataSource : ds.cloneWithRows(nextProps.videoDetails.searchSuggestion),
      videoDetails: nextProps.videoDetails });
  }

	handleSubmit(event) {
    this.setState({close: true});
    if(this.state.isConnected===false){
      Alert.alert("Please check your Internet connection!");
      return;
    }
		if(this.state.keyword==="")
			Alert.alert("Please enter something to search.");
		else{
			this.props.actions(this.state.keyword);
			this.props.navigator.push({
			  title: 'searchResult',
			  id : 'searchResult',
			});
		}

	}

  handleRowPress(rowData) {
    this.setState({keyword: rowData});
    this.props.actions(rowData);
    this.props.navigator.push({
      title: 'searchResult',
      id : 'searchResult',
    });
  }

  renderRow(rowData) {
    return (
      <View style={styles.rowButton}>
        <View style={styles.rowWrapper}>
          <TouchableOpacity onPress={() => this.handleRowPress(rowData)}>
            <Text>{ rowData }</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  async setValuesToLocalStorage(data, key){
    try {
      await AsyncStorage.removeItem(key);
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
      console.log(error);
    }
  }

  handleRecentPress(rowData) {
    let arr=[], flag=0;
    if(this.state.recentVideos!==null){
      arr=this.state.rows;
      for(let i=0; i<this.state.rows.length; i++)
        if(arr[i].videoId===rowData.videoId){
          arr[i].viewCount=arr[i].viewCount+1;
          flag=1;
        }
    }
    if(!flag){
      rowData.viewCount=1;
      arr.push(rowData);
    }
    sortByViews(arr);    
    this.setValuesToLocalStorage(arr, "recent").done(()=>{
      this.props.playVideo(rowData);
      this.props.navigator.push({
        title: 'videoDetail',
        id : 'videoDetail',
      });
    });
  }

  handleRemoveTabPress(rowData) {
    this.setState({open: false});
    let arr=[], arr2=[], flag=0;
    if(this.state.recentVideos!==null){
      arr=this.state.rows;
      for(let i=0; i<this.state.rows.length; i++)
        if(arr[i].videoId===rowData.videoId)
          continue;
        else
         arr2.push(arr[i])
    }

    sortByViews(arr2); 
    this.setValuesToLocalStorage(arr2, "recent").done(()=>{
      this.props.navigator.replace({
        title: 'Home',
        id : 'home',
      });
    });
  }

	render() {
    let self=this;
    var rowViews = this.state.rows.map(function(video, i){
      if(i<6){
        return (
          <View style={styles.row} key={video.videoId}>
            <TouchableOpacity
              delayLongPress={1000}
              onLongPress={() => {
                 self.props.autocomplete("");
                 self.setState({open: true, removeVideoId: video, close: true}) 
              }}
              onPress={() => self.handleRecentPress(video)}>
              <Image
                style={styles.thumbnail}
                source={{uri: video.mediumThumbnail}} />
              <Text numberOfLines={1}>{video.videoTitle}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    });

		return (
			<View style={styles.container}> 

          <FavouriteMenu  close={this.state.close} {...this.props}/>
           
          <View style={styles.list}>
            <Image
              style={styles.phoneLogo}
              source={require('../images/logo.png')} />
            <Text style={{fontSize: 25, marginTop:30}}>Search YouTube</Text> 
          </View> 
          
				
          <View style={styles.search}>
  					<TextInput
  						style={styles.searchInput}
  						value={this.state.keyword}
              onFocus={()=> this.setState({close: true})}      
  						onChangeText={(keyword) =>{ 
                this.setState({keyword, close: true});
                this.props.autocomplete(keyword);
              }}
  						onSubmitEditing={this.handleSubmit.bind(this)}
  						placeholder="Search for videos" />	
  					<Button  background={(Platform.OS === 'android') ? 
  							TouchableNativeFeedback.Ripple('#f39c12', true) : null} 
  							style={styles.searchButton}
  							onPress={this.handleSubmit.bind(this)}>
  						<Image
  						  	style={styles.icon}
  						  	source={require('../images/search.png')} />
  					</Button>
  				</View>
            
            <Modal
               open={this.state.open}
               style={{alignItems: 'center'}}>
               <View>
                  <TouchableOpacity
                     onPress={() => this.setState({open: false, removeVideoId: null})}>
                     <Image
                       style={styles.close}
                       source={require('../images/close.png')} />
                  </TouchableOpacity>
                  <TouchableOpacity
                     style={{margin: 5}}
                     onPress={() =>this.handleRemoveTabPress(this.state.removeVideoId)}>
                     <Text>Remove</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                     style={{margin: 5}}
                     onPress={() =>this.handleRecentPress(this.state.removeVideoId)}>
                     <Text>Open</Text>
                  </TouchableOpacity>
               </View>
            </Modal> 

            <View style={styles.rowWrapper}>
               {this.props.videoDetails.searchSuggestion.length>0 && this.state.keyword.length>0 ?
                 <ListView
                   dataSource={this.state.dataSource}
                   renderRow={this.renderRow.bind(this)}/>: null}
            </View>

            <View style={styles.list}>
               {rowViews}
            </View> 

			</View>
		)
	}
}



var styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#F1F1F1',
		padding: 10
	},
  phoneLogo: {
    height: 90,
    width: 90,
    margin:10
  },
	search: {		
    flexDirection: 'row',
		alignSelf: 'center'
	},
	searchButton: {
		borderColor: 'transparent',
		backgroundColor: '#ffffff',
		borderRadius: 0,
		borderWidth: 3,
		padding:10,
      height:60
	},
	searchInput: {
		backgroundColor: '#ffffff',
		padding: 10,	
		borderRadius: 0,
		flex:1,
      height:60
	},
	icon: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		height: 30,
		width: 30
	},
  thumbnail: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      height: 65,
      width: 86,
  },
  close: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: 'flex-end'
  },
  items: {
    flex: 1,
    backgroundColor: '#F1F1F1',
    padding: 10,
  },
  rowButton: {
    padding: 10,
  },
  rowWrapper: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex:1
  },
  menuTrigger: {
    flexDirection: 'row',
    paddingHorizontal: 10
  },
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop:5
  },
  row: {  
    margin:13,
    width: 86,
    height: 70
  },
})

export default connect(state =>({
    videoDetails: state.videoDetails
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchYouTubeAPI, dispatch),
    autocomplete: bindActionCreators(autocomplete,dispatch),
    playVideo: bindActionCreators(playVideo,dispatch)
  })
)(Search);
