'use strict'
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  ListView,
  Image,
  StyleSheet,
  Alert,
  AsyncStorage
} from 'react-native';

import Relay, { 
  Route,
  RootContainer,
  DefaultNetworkLayer
} from 'react-relay';

import RenderRow from '../components/renderRow';
import { sortByLikesPercentage, sortByViews } from '../components/globalFunctions';

Relay.injectNetworkLayer(new DefaultNetworkLayer('http://172.18.4.101:3000/graphql'))


export class UserRoute extends Route {
  static paramDefinitions = {
    Keyword: { required: true }
  }
  static queries = {
    user: () => Relay.QL`
      query { 
        user(keyword: $Keyword)
      }
    `
  }
  static routeName = 'UserRoute'
}

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export class UserInfo extends Component {

  constructor(props) {
      super(props);
      let user = this.props.user;
      this.props.searchResult(user.items);
      this.state = { 
        dataSource: ds.cloneWithRows([]), 
        favourites: this.props.videoDetails.favourites
      };
  }    

  componentWillReceiveProps(nextProps) {
    this.setState({ 
      dataSource: ds.cloneWithRows(nextProps.videoDetails.searchItems),
      favourites: nextProps.videoDetails.favourites 
    });
  }


  renderRow(rowData) {
    return (
      <RenderRow routePage="searchResult" favourites={this.state.favourites} rowData={rowData} {...this.props}/>
    )
  }

  render () {
    return (
		<View>
		    <ListView
		      style={styles.items}
		      dataSource={this.state.dataSource}
		      renderRow={this.renderRow.bind(this)}/>
		</View>
    );
  }
}

UserInfo = Relay.createContainer(UserInfo, {
  fragments: {
    user: () => Relay.QL`
      fragment on User {
        items{
      		videoId,
          videoTitle,
      		mediumThumbnail,
          publishDateTime,
      		statistics{likes,dislikes,views}}
      }
    `
  }
})

const styles = StyleSheet.create({
  items: {
    flex: 1,
    backgroundColor: '#F1F1F1',
    paddingLeft: 10,
    paddingRight:10
  }
})

